#ifndef Darwin_h
#define Darwin_h

#include <vector>   // vector
#include <iostream> // ostream

#include "Creature.hpp" // Creature

using namespace std;

class Darwin
{
private:
    vector<Creature> creatures;
    vector<vector<Creature *>> grid;
    int rows;
    int cols;

public:
    Darwin(int rows, int cols, int num_creatures);
    void addCreature(Creature &c);
    bool space_is_empty(int row, int col);
    bool space_is_enemy(int row, int col, Creature &current);
    bool space_is_wall(int row, int col);
    void moveCreature(int old_row, int old_col, int new_row, int new_col);
    void printGrid(ostream &sout);
    void runSim();
    void sortCreatures();
    void removeDuplicates();
    friend class Creature;
};

#endif