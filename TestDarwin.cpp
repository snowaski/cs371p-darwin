// ---------------
// TestDarwin.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include "gtest/gtest.h"

#define private public

#include "Darwin.hpp"
#include "Creature.hpp"

using namespace std;

// --------
// DARWIN
// --------

TEST(DarwinFixture, empty_space0)
{
    Darwin dar(5, 5, 1);

    Creature c(nullptr, 'f', 0, 1, 'e');

    dar.grid[0][1] = &c;

    ASSERT_EQ(dar.space_is_empty(0, 0), true);
}

TEST(DarwinFixture, empty_space1)
{
    Darwin dar(5, 5, 1);

    Creature c(nullptr, 'f', 0, 1, 'e');

    dar.grid[0][1] = &c;

    ASSERT_EQ(dar.space_is_empty(0, 1), false);
}

TEST(DarwinFixture, is_enemy0)
{
    Darwin dar(5, 5, 2);

    Species s;

    Creature c1(nullptr, 'f', 0, 0, 'e');
    Creature c2(&s, 'w', 0, 1, 'w');

    dar.grid[0][0] = &c1;
    dar.grid[0][1] = &c2;

    ASSERT_EQ(dar.space_is_enemy(0, 1, c1), true);
}

TEST(DarwinFixture, is_enemy1)
{
    Darwin dar(5, 5, 2);

    Creature c1(nullptr, 'f', 0, 0, 'e');
    Creature c2(nullptr, 'w', 0, 1, 'w');

    dar.grid[0][0] = &c1;
    dar.grid[0][1] = &c2;

    ASSERT_EQ(dar.space_is_enemy(0, 1, c1), false);
}

TEST(DarwinFixture, is_enemy2)
{
    Darwin dar(5, 5, 1);

    Creature c1(nullptr, 'f', 0, 0, 'e');

    dar.grid[0][0] = &c1;

    ASSERT_EQ(dar.space_is_enemy(3, 3, c1), false);
}

TEST(DarwinFixture, move)
{
    Darwin dar(5, 5, 1);

    Creature c1(nullptr, 'f', 0, 0, 'e');

    dar.grid[0][0] = &c1;

    dar.moveCreature(0, 0, 0, 1);

    ASSERT_EQ(dar.grid[0][0], nullptr);
    ASSERT_EQ(dar.grid[0][1], &c1);
}

TEST(DarwinFixture, printGrid)
{
    ostringstream sout;

    Darwin dar(2, 2, 1);

    Creature c1(nullptr, 'f', 0, 0, 'e');

    dar.grid[0][0] = &c1;

    dar.printGrid(sout);

    ASSERT_EQ(sout.str(), "  01\n0 f.\n1 ..\n");
}

// --------
// CREATURE
// --------

TEST(DarwinFixture, left0)
{
    Creature c(nullptr, 'f', 0, 0, 'e');

    c.left();

    ASSERT_EQ(c.dir, 'n');
}

TEST(DarwinFixture, left1)
{
    Creature c(nullptr, 'f', 0, 0, 's');

    c.left();

    ASSERT_EQ(c.dir, 'e');
}

TEST(DarwinFixture, left2)
{
    Creature c(nullptr, 'f', 0, 0, 'w');

    c.left();

    ASSERT_EQ(c.dir, 's');
}

TEST(DarwinFixture, left3)
{
    Creature c(nullptr, 'f', 0, 0, 'n');

    c.left();

    ASSERT_EQ(c.dir, 'w');
}

TEST(DarwinFixture, right0)
{
    Creature c(nullptr, 'f', 0, 0, 'e');

    c.right();

    ASSERT_EQ(c.dir, 's');
}

TEST(DarwinFixture, right1)
{
    Creature c(nullptr, 'f', 0, 0, 's');

    c.right();

    ASSERT_EQ(c.dir, 'w');
}

TEST(DarwinFixture, right2)
{
    Creature c(nullptr, 'f', 0, 0, 'w');

    c.right();

    ASSERT_EQ(c.dir, 'n');
}

TEST(DarwinFixture, right3)
{
    Creature c(nullptr, 'f', 0, 0, 'n');

    c.right();

    ASSERT_EQ(c.dir, 'e');
}

TEST(DarwinFixture, hop0)
{
    Darwin dar(2, 2, 1);

    Creature c(nullptr, 'f', 0, 0, 's');

    dar.grid[0][0] = &c;

    c.hop(&dar);

    ASSERT_EQ(c.row, 1);
    ASSERT_EQ(c.col, 0);
    ASSERT_EQ(c.dir, 's');
    ASSERT_EQ(c.program_counter, 1);
}

TEST(DarwinFixture, hop1)
{
    Darwin dar(2, 2, 1);

    Creature c(nullptr, 'f', 0, 0, 'n');

    dar.grid[0][0] = &c;

    c.hop(&dar);

    ASSERT_EQ(c.row, 0);
    ASSERT_EQ(c.col, 0);
    ASSERT_EQ(c.dir, 'n');
    ASSERT_EQ(c.program_counter, 1);
}

TEST(DarwinFixture, infect0)
{
    Darwin dar(2, 2, 2);

    Species s;

    Creature c1(nullptr, 'f', 0, 0, 'n');
    Creature c2(&s, 's', 0, 1, 'w');

    dar.grid[0][0] = &c1;
    dar.grid[0][1] = &c2;

    c2.infect(&dar);

    ASSERT_EQ(c2.row, 0);
    ASSERT_EQ(c2.col, 1);
    ASSERT_EQ(c2.dir, 'w');
    ASSERT_EQ(c2.program_counter, 1);

    ASSERT_EQ(c1.row, 0);
    ASSERT_EQ(c1.col, 0);
    ASSERT_EQ(c1.dir, 'n');
    ASSERT_EQ(c1.program_counter, 0);
    ASSERT_EQ(c1.species, &s);
    ASSERT_EQ(c1.name, 's');
}

TEST(DarwinFixture, infect1)
{
    Darwin dar(2, 2, 2);

    Species s;

    Creature c1(nullptr, 'f', 0, 0, 'n');
    Creature c2(&s, 's', 0, 1, 'n');

    dar.grid[0][0] = &c1;
    dar.grid[0][1] = &c2;

    c2.infect(&dar);

    ASSERT_EQ(c2.row, 0);
    ASSERT_EQ(c2.col, 1);
    ASSERT_EQ(c2.dir, 'n');
    ASSERT_EQ(c2.program_counter, 1);

    ASSERT_EQ(c1.row, 0);
    ASSERT_EQ(c1.col, 0);
    ASSERT_EQ(c1.dir, 'n');
    ASSERT_EQ(c1.program_counter, 0);
    ASSERT_EQ(c1.species, nullptr);
    ASSERT_EQ(c1.name, 'f');
}

TEST(DarwinFixture, go_n)
{
    Darwin dar(2, 2, 2);

    Species s;
    s.addInstruction(instruction::LEFT);
    s.addInstruction(instruction::GO_0);

    Creature c(&s, 's', 0, 1, 'n');

    dar.grid[0][1] = &c;

    c.go_n(&dar, 0);

    ASSERT_EQ(c.row, 0);
    ASSERT_EQ(c.col, 1);
    ASSERT_EQ(c.dir, 'w');
    ASSERT_EQ(c.program_counter, 1);
}

TEST(DarwinFixture, if_enemy0)
{
    Darwin dar(2, 2, 2);

    Species s;
    s.addInstruction(instruction::LEFT);
    s.addInstruction(instruction::GO_0);
    s.addInstruction(instruction::RIGHT);

    Creature c1(nullptr, 'f', 0, 0, 'n');
    Creature c2(&s, 's', 0, 1, 'w');

    dar.grid[0][0] = &c1;
    dar.grid[0][1] = &c2;

    c2.if_enemy_n(&dar, 0);

    ASSERT_EQ(c2.row, 0);
    ASSERT_EQ(c2.col, 1);
    ASSERT_EQ(c2.dir, 's');
    ASSERT_EQ(c2.program_counter, 1);
}

TEST(DarwinFixture, if_enemy1)
{
    Darwin dar(2, 2, 2);

    Species s;
    s.addInstruction(instruction::LEFT);
    s.addInstruction(instruction::RIGHT);
    s.addInstruction(instruction::GO_0);

    Creature c1(&s, 's', 0, 1, 'w');

    dar.grid[0][1] = &c1;

    c1.if_enemy_n(&dar, 0);

    ASSERT_EQ(c1.row, 0);
    ASSERT_EQ(c1.col, 1);
    ASSERT_EQ(c1.dir, 'n');
    ASSERT_EQ(c1.program_counter, 2);
}

TEST(DarwinFixture, if_empty0)
{
    Darwin dar(2, 2, 2);

    Species s;
    s.addInstruction(instruction::LEFT);
    s.addInstruction(instruction::RIGHT);
    s.addInstruction(instruction::GO_0);

    Creature c1(&s, 's', 0, 1, 'n');

    dar.grid[0][1] = &c1;

    c1.if_empty_n(&dar, 0);

    ASSERT_EQ(c1.row, 0);
    ASSERT_EQ(c1.col, 1);
    ASSERT_EQ(c1.dir, 'e');
    ASSERT_EQ(c1.program_counter, 2);
}

TEST(DarwinFixture, if_empty1)
{
    Darwin dar(2, 2, 2);

    Species s;
    s.addInstruction(instruction::LEFT);
    s.addInstruction(instruction::RIGHT);
    s.addInstruction(instruction::GO_0);

    Creature c1(&s, 's', 0, 1, 's');

    dar.grid[0][1] = &c1;

    c1.if_empty_n(&dar, 0);

    ASSERT_EQ(c1.row, 0);
    ASSERT_EQ(c1.col, 1);
    ASSERT_EQ(c1.dir, 'e');
    ASSERT_EQ(c1.program_counter, 1);
}

TEST(DarwinFixture, if_random0)
{
    srand(0);
    Darwin dar(2, 2, 2);

    Species s;
    s.addInstruction(instruction::LEFT);
    s.addInstruction(instruction::RIGHT);
    s.addInstruction(instruction::GO_0);

    Creature c1(&s, 's', 0, 1, 'n');

    dar.grid[0][1] = &c1;

    c1.if_random_n(&dar, 0);

    ASSERT_EQ(c1.row, 0);
    ASSERT_EQ(c1.col, 1);
    ASSERT_EQ(c1.dir, 'w');
    ASSERT_EQ(c1.program_counter, 1);
}

TEST(DarwinFixture, if_random1)
{
    srand(2);
    Darwin dar(2, 2, 2);

    Species s;
    s.addInstruction(instruction::LEFT);
    s.addInstruction(instruction::RIGHT);
    s.addInstruction(instruction::GO_0);

    Creature c1(&s, 's', 0, 1, 'n');

    dar.grid[0][1] = &c1;

    c1.if_random_n(&dar, 0);

    ASSERT_EQ(c1.row, 0);
    ASSERT_EQ(c1.col, 1);
    ASSERT_EQ(c1.dir, 'e');
    ASSERT_EQ(c1.program_counter, 2);
}