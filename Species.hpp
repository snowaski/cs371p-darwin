#ifndef Species_h
#define Species_h

#include <vector> // vector
#include <string> // string
using namespace std;

class Darwin;
class Creature;
#include "Darwin.hpp"
#include "Creature.hpp"

enum instruction
{
    HOP,
    LEFT,
    RIGHT,
    INFECT,
    IF_EMPTY_7,
    IF_WALL,
    IF_RANDOM_5,
    IF_ENEMY_3,
    IF_ENEMY_9,
    GO_0
};

class Species
{
private:
    vector<instruction> program;

public:
    Species() = default;
    void addInstruction(instruction i);
    void executeInstruction(Darwin *dar, Creature &c, int program_counter);
};

#endif