#include "Species.hpp" // Species"

void Species::addInstruction(instruction i)
{
    program.push_back(i);
}

void Species::executeInstruction(Darwin *dar, Creature &c, int program_counter)
{
    instruction i = program[program_counter];
    switch (i)
    {
    case HOP:
        c.hop(dar);
        break;
    case LEFT:
        c.left();
        break;
    case RIGHT:
        c.right();
        break;
    case GO_0:
        c.go_n(dar, 0);
        break;
    case INFECT:
        c.infect(dar);
        break;
    case IF_EMPTY_7:
        c.if_empty_n(dar, 7);
        break;
    case IF_ENEMY_9:
        c.if_enemy_n(dar, 9);
        break;
    case IF_ENEMY_3:
        c.if_enemy_n(dar, 3);
        break;
    case IF_RANDOM_5:
        c.if_random_n(dar, 5);
        break;
    default:
        break;
    }
}