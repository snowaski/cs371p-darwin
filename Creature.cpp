#include <tuple>

#include "Creature.hpp" // Creature

// gets the next square based on the creature's direction
pair<int, int> Creature::getNextSquare()
{
    int new_row = row;
    int new_col = col;
    switch (dir)
    {
    case 'e':
        new_col += 1;
        break;
    case 's':
        new_row += 1;
        break;
    case 'w':
        new_col -= 1;
        break;
    case 'n':
        new_row -= 1;
        break;
    default:
        break;
    }

    return make_pair(new_row, new_col);
}

Creature::Creature(Species *s, char n, int r, int c, char d)
{
    name = n;
    row = r;
    col = c;
    dir = d;
    species = s;
    program_counter = 0;
}

void Creature::left()
{
    switch (dir)
    {
    case 'e':
        dir = 'n';
        break;
    case 'n':
        dir = 'w';
        break;
    case 'w':
        dir = 's';
        break;
    case 's':
        dir = 'e';
        break;
    default:
        break;
    }
    ++program_counter;
}

void Creature::right()
{
    switch (dir)
    {
    case 'e':
        dir = 's';
        break;
    case 's':
        dir = 'w';
        break;
    case 'w':
        dir = 'n';
        break;
    case 'n':
        dir = 'e';
        break;
    default:
        break;
    }
    ++program_counter;
}

// moves a creature forward one space if square is empty
void Creature::hop(Darwin *dar)
{
    int new_row, new_col;
    tie(new_row, new_col) = getNextSquare();

    if (dar->space_is_empty(new_row, new_col))
    {
        dar->moveCreature(row, col, new_row, new_col);
        row = new_row;
        col = new_col;
    }
    ++program_counter;
}

// converts a crature of a different species to this creature's species
void Creature::infect(Darwin *dar)
{
    int new_row, new_col;
    tie(new_row, new_col) = getNextSquare();

    if (dar->space_is_enemy(new_row, new_col, *this))
    {
        dar->grid[new_row][new_col]->species = species;
        dar->grid[new_row][new_col]->name = name;
        dar->grid[new_row][new_col]->program_counter = 0;
    }
    program_counter++;
}

// sets program counter to n
void Creature::go_n(Darwin *d, int n)
{
    program_counter = n;
    turn(d);
}

// if there is an enemy in the next square, set program counter to n
void Creature::if_enemy_n(Darwin *dar, int n)
{
    int new_row, new_col;
    tie(new_row, new_col) = getNextSquare();

    if (dar->space_is_enemy(new_row, new_col, *this))
    {
        program_counter = n;
    }
    else
        ++program_counter;
    turn(dar);
}

// if there is a wall in the next square, set program counter to n
void Creature::if_wall_n(Darwin *dar, int n)
{
    int new_row, new_col;
    tie(new_row, new_col) = getNextSquare();

    if (dar->space_is_wall(new_row, new_col))
    {
        program_counter = n;
    }
    else
        ++program_counter;
    turn(dar);
}

// randomly turn left or right
void Creature::if_random_n(Darwin *dar, int n)
{
    if (rand() % 2 == 0)
    {
        program_counter++;
    }
    else
    {
        program_counter = n;
    }
    turn(dar);
}

// if the space ahead is empty, set program counter to n
void Creature::if_empty_n(Darwin *dar, int n)
{
    int new_row, new_col;
    tie(new_row, new_col) = getNextSquare();

    if (dar->space_is_empty(new_row, new_col))
    {
        program_counter = n;
    }
    else
        ++program_counter;
    turn(dar);
}

void Creature::turn(Darwin *dar)
{
    species->executeInstruction(dar, *this, program_counter);
}
