#include <algorithm>

using namespace std;

#include "Darwin.hpp"

Darwin::Darwin(int rs, int cs, int num_creatures) : grid(rs, vector<Creature *>(cs, nullptr))
{
    creatures.reserve(num_creatures);

    rows = rs;
    cols = cs;
}

void Darwin::addCreature(Creature &c)
{
    creatures.push_back(c);
}

bool Darwin::space_is_empty(int row, int col)
{
    return row >= 0 && col >= 0 && row < rows && col < cols && grid[row][col] == nullptr;
}

bool Darwin::space_is_wall(int row, int col)
{
    return row < 0 || col < 0 || row >= rows || col >= cols;
}

bool Darwin::space_is_enemy(int row, int col, Creature &current)
{
    return row >= 0 && col >= 0 && row < rows && col < cols && grid[row][col] != nullptr && grid[row][col]->species != current.species;
}

void Darwin::moveCreature(int old_row, int old_col, int new_row, int new_col)
{
    grid[new_row][new_col] = grid[old_row][old_col];
    grid[old_row][old_col] = nullptr;
}

void Darwin::runSim()
{
    for (Creature &c : creatures)
    {
        c.turn(this);
    }
}

// overwrites creatures that share the same space
void Darwin::removeDuplicates()
{
    vector<Creature>::iterator it = creatures.begin();
    ++it;
    while (it != creatures.end())
    {
        if (*it == *(it - 1))
        {
            creatures.erase(it - 1);
        }
        else
            ++it;
    }
}

// sort creatures in top down, left to right order
void Darwin::sortCreatures()
{
    stable_sort(creatures.begin(), creatures.end());

    for (int i = 0; i < creatures.size(); i++)
    {
        Creature c = creatures[i];
        grid[c.row][c.col] = &creatures[i];
    }
}

void Darwin::printGrid(ostream &sout)
{
    sout << "  ";
    for (int c = 0; c < cols; c++)
    {
        sout << c % 10;
    }
    sout << endl;
    for (int r = 0; r < rows; r++)
    {
        sout << r % 10 << " ";
        for (int c = 0; c < cols; c++)
        {
            if (grid[r][c] == nullptr)
            {
                sout << ".";
            }
            else
            {
                sout << grid[r][c]->name;
            }
        }
        sout << endl;
    }
}
