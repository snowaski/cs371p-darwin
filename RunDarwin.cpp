// --------------
// RunDarwin.c++
// --------------

// --------
// includes
// --------

#include <iostream> // cin, cout
#include <sstream>  // istringstream

#include "Darwin.hpp"
#include "Creature.hpp"
#include "Species.hpp"

using namespace std;

// ----
// main
// ----

int main()
{
    // create species

    Species food;
    food.addInstruction(instruction::LEFT);
    food.addInstruction(instruction::GO_0);

    Species hopper;
    hopper.addInstruction(instruction::HOP);
    hopper.addInstruction(instruction::GO_0);

    Species rover;
    rover.addInstruction(instruction::IF_ENEMY_9);
    rover.addInstruction(instruction::IF_EMPTY_7);
    rover.addInstruction(instruction::IF_RANDOM_5);
    rover.addInstruction(instruction::LEFT);
    rover.addInstruction(instruction::GO_0);
    rover.addInstruction(instruction::RIGHT);
    rover.addInstruction(instruction::GO_0);
    rover.addInstruction(instruction::HOP);
    rover.addInstruction(instruction::GO_0);
    rover.addInstruction(instruction::INFECT);
    rover.addInstruction(instruction::GO_0);

    Species trap;
    trap.addInstruction(instruction::IF_ENEMY_3);
    trap.addInstruction(instruction::LEFT);
    trap.addInstruction(instruction::GO_0);
    trap.addInstruction(instruction::INFECT);
    trap.addInstruction(instruction::GO_0);

    string test_cases;
    getline(cin, test_cases);
    int num_test_cases = stoi(test_cases);

    int test = 0;
    string s;

    while (test < num_test_cases)
    {
        srand(0);
        getline(cin, s); // blank line

        // get the dimensions of the grid
        getline(cin, s);
        istringstream dim_string(s);
        getline(dim_string, s, ' ');
        int rows = stoi(s);
        getline(dim_string, s, ' ');
        int cols = stoi(s);

        getline(cin, s);
        int num_creatures = stoi(s);

        Darwin dar(rows, cols, num_creatures);

        // get the information of creatures
        int c = 0;
        while (c < num_creatures)
        {
            getline(cin, s);
            istringstream sin(s);

            string creature_name;
            getline(sin, creature_name, ' ');

            getline(sin, s, ' ');
            int creature_row = stoi(s);

            getline(sin, s, ' ');
            int creature_col = stoi(s);

            string creature_dir;
            getline(sin, creature_dir, ' ');

            Species *species = nullptr;

            switch (creature_name[0])
            {
            case 'f':
                species = &food;
                break;
            case 'h':
                species = &hopper;
                break;
            case 'r':
                species = &rover;
                break;
            case 't':
                species = &trap;
                break;
            default:
                break;
            }

            Creature creature(species, creature_name[0], creature_row, creature_col, creature_dir[0]);
            dar.addCreature(creature);

            ++c;
        }

        // determine turn order and remove creatures occupying same square
        dar.sortCreatures();
        dar.removeDuplicates();

        getline(cin, s);
        istringstream settings_string(s);

        getline(settings_string, s, ' ');
        int num_sims = stoi(s);

        getline(settings_string, s, ' ');
        int freq = stoi(s);

        // run simulations
        cout << "*** Darwin " << rows << "x" << cols << " ***" << endl;
        for (int i = 0; i <= num_sims; ++i)
        {
            dar.sortCreatures();
            if (i % freq == 0)
            {
                cout << "Turn = " << i << "." << endl;
                dar.printGrid(cout);
                if (i != (num_sims / freq) * freq || test < num_test_cases - 1)
                {
                    cout << endl;
                }
            }
            dar.runSim();
        }

        ++test;
    }
    return 0;
}
