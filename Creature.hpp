#ifndef Creature_h
#define Creature_h

#include <iostream> // ostream
#include <utility>  // make_pair, pair

using namespace std;

class Species;
#include "Species.hpp" // Species
#include "Darwin.hpp"  // Darwin

class Creature
{
private:
    char dir;
    Species *species;
    int row;
    int col;
    int program_counter;
    char name;

public:
    Creature() = default;
    Creature(Species *s, char name, int row, int col, char dir);
    void left();
    void right();
    void hop(Darwin *dar);
    void infect(Darwin *dar);
    void go_n(Darwin *d, int n);
    void if_enemy_n(Darwin *dar, int n);
    void if_empty_n(Darwin *dar, int n);
    void if_wall_n(Darwin *dar, int n);
    void if_random_n(Darwin *dar, int n);
    void turn(Darwin *dar);
    bool operator<(const Creature &c) const { return row < c.row || (row == c.row && col < c.col); }
    bool operator==(const Creature &c) const { return row == c.row && col == c.col; };
    pair<int, int> getNextSquare();
    friend class Darwin;
};

#endif
